"use strict";

class LD_Error {
    /**
     * JSON-LD Hydra error
     * @param {string} title
     * @param {string} description
     * @returns {LD_Error}
     * @constructor
     */
    constructor(title, description) {
        this['@context'] = 'http://www.w3.org/ns/hydra/context.jsonld';
        this['@type'] = 'Error';
        this.title = title;
        this.description = description;
    }

    stringify(){
        return JSON.stringify(this);
    }
}

module.exports = LD_Error;