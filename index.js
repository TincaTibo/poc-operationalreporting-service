#!/usr/bin/env node
"use strict";

const co = require('co');

const Logger = require('./config/logger');
const Config = require('./config/config');
const env = require('./config/env');
const Routes = require('./http/routes');
const Dao = require('./dao/monitor-dao');

const log = Logger(env.applicationName);

co(function * (){
    const config = yield Config.initConfig(env.applicationName, log);

    yield Dao.init({
        code: 'ITEMS',
        store: config.items.store,
        log: config.log,
        stats: config.stats
    });

    const server = yield Routes.initRoutes(config);
    log.info(`${env.applicationName} listening on port ${server.address().port}!`);

    process.on('SIGTERM', () => {
        server.close(() => {
            setTimeout(() => {
                log.info(`${env.applicationName} exited!`);
                process.exit(0);
            }, 2000)
        });
    });
}).catch(err => {
    log.error(err, `Couldn't launch application ${env.applicationName}`);
    process.exit(1);
});