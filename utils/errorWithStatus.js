const title = {
    400: "Bad request",
    401: "Unauthorized access",
    403: "Access Forbidden",
    404: "Item not found",
    405: "Method not allowed",
    406: "Not acceptable",
    408: "A downstream request has timed-out",
    409: "The request is in conflict with server state",
    410: "Gone",
    411: "Length required",
    412: "Precondition failed",
    413: "Request entity too large",
    414: "Request-uri too large",
    415: "Unsupported media type",
    416: "Requested range not satisfiable",
    417: "Expectation failed",
    422: "Unprocessable entity",
    423: "Locked",
    424: "Failed dependency",
    425: "Unordered collection",
    426: "Upgrade required",
    428: "Precondition required",
    429: "Too many requests",
    431: "Request header fields too large"
};

errorTitle = (status) => {
    return title[status];
};

module.exports.errorTitle = errorTitle;