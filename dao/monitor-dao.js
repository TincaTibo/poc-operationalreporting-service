"use strict";

const elasticSearch = require('elasticsearch');
const moment = require('moment');
const _ = require('lodash');
const Q = require('q');
const Brakes = require('brakes');

const log = require('../config/logger');

let daos = {};

class MonitorDAO{
    constructor(source) {
        _.assign(this, source);
        daos[this.code] = this;

        this.searchItemsFromES = new Brakes(
            (body) => this.client.search({
                index: this.indexGet,
                type: this.type,
                body,
                requestTimeout: this.getTimeOutMs,
                ignore: [404],
            }),
            {
                name: 'Search ' + this.code,
                group: 'ES-' + this.code,
                timeout: this.getTimeOutMs,
                circuitDuration: this.circuitDurationMs,
                threshold: this.circuitThreshold,
                statInterval: this.stats.intervalMs,
                bucketSpan: this.stats.bucketSpanMs,
                bucketNum: this.stats.bucketCount,
                isPromise: true,
            }
        );
    }

    static *init(config){
        const indexGet = config.store.indexGet || 'spider';
        const type = config.store.type || 'doc';
        const park = config.store.park;

        const client = new elasticSearch.Client({
            host: {
                protocol: config.store.protocol || 'http',
                host: config.store.server || 'localhost',
                port: config.store.port || 9200,
                auth: config.store.auth || undefined,
            },
            httpAuth: config.store.auth,
            sniffOnStart: true,
            sniffInterval: 60000,
            sniffOnConnectionFault: true,
        });

        let nbRetries = 0, res;
        const maxRetries = config.store.connectRetryTimes;

        do{
            try{
                res = yield client.ping({
                    requestTimeout: moment.duration(config.store.connectTimeOut).asMilliseconds()
                });
            }
            catch(e){
                nbRetries++;
                config.log.warn(`Warning: Could not connect to ElasticSearch ${config.store.server}: ${e.message}. Retrying  ${nbRetries}/${maxRetries}...`);
                yield Q.delay(moment.duration(config.store.connectRetryDelay).asMilliseconds());

                if(nbRetries > maxRetries){
                    throw e;
                }
            }
        }
        while(!res);
        return new MonitorDAO({
            client,
            indexGet,
            type,
            timeField: config.store.timeField || 'creationDate',
            code: config.code,
            log: config.log,
            park,
            getTimeOutMs: moment.duration(config.store.getTimeOut).asMilliseconds(),
            stats: {
            intervalMs: moment.duration(config.stats.interval).asMilliseconds(),
                bucketSpanMs: moment.duration(config.stats.bucketSpan).asMilliseconds(),
                bucketCount: config.stats.bucketCount,
            },
            circuitDurationMs: moment.duration(config.store.circuitDuration).asMilliseconds(),
            circuitThreshold: moment.duration(config.store.circuitThreshold).asMilliseconds(),
        });
    }

    *search({startTime, stopTime, sort, query, aggs, size, next}){
        if(query){
            query.replace('@id','_id');
        }
        let queryES = {
            size: size,
            query: { bool: { filter: { bool: {
                must: [
                    { range: { [this.timeField]: { lte: moment.unix(stopTime).toISOString() } } },
                    { range: { [this.timeField]: { gte: moment.unix(startTime).toISOString() } } },
                    { term: { park : { value: this.park } } },
                    { query_string: {
                            query: query,
                            analyze_wildcard: true,
                            default_operator: "AND",
                            lenient: true
                        } }
                ] } } }
            }
        };
        if(!stopTime){
            queryES.query.bool.filter.bool.must.splice(0,1);
        }
        if(!startTime){
            queryES.query.bool.filter.bool.must.splice(1,1);
        }
        if(!query){
            queryES.query.bool.filter.bool.must.pop();
        }
        if(aggs){
            queryES.aggs = aggs;
        }
        if(sort){
            queryES.sort = sort.map(s => ({
                [s.key]: {
                    order: s.order
                }
            }));
        }
        if(next){
            queryES.search_after = next;
        }

        const resp = yield this.searchItemsFromES.exec(queryES);

        if(resp.hits.total === 0){
            return {
                total: 0,
                aggs: null,
                items: []
            };
        }
        else{
            const items = resp.hits.hits;

            return {
                total: resp.hits.total,
                aggs: resp.aggregations || null,
                items: items.map(h => {
                    const item = h['_source'];
                    if(!item['@id']){
                        item['@id'] = h._id;
                    }
                    return h['_source']
                }),
                lastSort: sort && items.length ? items[items.length-1].sort : null
            };
        }
    }

    end(){
    }

    static getInstance(code){
        return daos[code];
    }
}

module.exports = MonitorDAO;