FROM alpine:latest

RUN echo -e "http://dl-cdn.alpinelinux.org/alpine/edge/community/\nhttp://dl-cdn.alpinelinux.org/alpine/edge/main/" >> /etc/apk/repositories
RUN apk add --no-cache nodejs-current

WORKDIR /app
COPY . /app
ARG registry
RUN apk --no-cache add --virtual build-dependencies ca-certificates nodejs-current-npm \
    && npm install --quiet --production --no-progress --registry=${registry:-https://registry.npmjs.org} \
    && npm cache clean --force \
    && apk del build-dependencies nodejs-current-npm

ARG port
EXPOSE $port
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["index.js"]