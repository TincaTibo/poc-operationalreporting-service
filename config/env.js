module.exports = {
    applicationName: process.env.APPLICATION_NAME || 'poc-read',
    uriPrefix: process.env.APPLICATION_URI_PREFIX || 'poc',
};
