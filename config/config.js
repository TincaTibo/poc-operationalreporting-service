"use strict";

const _ = require('lodash');
const FS = require('q-io/fs');

/**
 * Current config
 * @static
 * @type {Config}
 */
let config = null;

/**
 * Configuration fetcher for Webstreams
 * @class
 */
class Config{
    constructor(source){
        _.assign(this, source);
        this.jwt.pubKey = new Buffer(source.jwt.pubKey,'base64');

        config = this;
    }

    static *initConfig(applicationName, log){
        const conf = yield FS.read(`config.json`);

        const pubKey = yield FS.read(process.env.JWT_PUBLIC_KEY_SECRET);
        const jwt = {
            pubKey: Buffer.from(pubKey),
        };

        return new Config(_.assign(JSON.parse(conf), {applicationName, log, jwt}));
    }

    static getInstance () {
        return config;
    }
}

module.exports = Config;