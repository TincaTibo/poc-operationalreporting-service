/**
 * Module exposing search http communications routes routines
 * @module http/search-http-com-service
 * @author TincaTibo@gmail.com
 * @type {LD_Error|exports|module.exports}
 */
"use strict";

const _ = require('lodash');
const Joi = require('joi');

const DAO = require('../dao/monitor-dao');
const env = require('../config/env');

exports.checkRequest = function * (next) {
    const req = this.request;

    //Body-parser did not get the body
    if (!Object.keys(req.body).length) {
        if (!req.is('application/json')) {
            this.throw(415, `Wrong request type, only application/json is accepted.`);
        }
        else { //Empty body
            this.throw(411, `The request body is empty.`);
        }
    }

    //Check for accepted content-type
    if (!req.accepts('application/ld+json','application/json')) {
        this.throw(406, `Cannot provide requested content type. Only json/json-ld is possible.`);
    }

    const schema = Joi.object().keys({
        startTime: Joi.number(),
        stopTime: Joi.number(),
        query: Joi.string().allow('',null),
        aggs: Joi.object(),
        sort: Joi.array().items(Joi.object().keys({
            key: Joi.string().required(),
            order: Joi.string().valid(['asc','desc']).required(),
        }).required()).allow(null),
        next: Joi.array(),
        size: Joi.number().integer().greater(-1).less(100).required(),
    }).required();

    const validationError = Joi.validate(req.body, schema, { allowUnknown: true }).error;
    if(validationError){
        this.throw(422, `Received JSON did not correspond to expected query pattern: ${validationError}`);
    }

    yield next;
};

exports.processRequest = (code) => function *(next) {
    const req = this.request;
    const res = this.response;
    const log = this.app.context.log;

    const query = req.body;
    const results = yield DAO.getInstance(code).search(query);
    log.debug(`Found ${results.total} items matching search.`);

    //If query was sorted, prepare nextPage hypermedia
    if(results.lastSort){
        const nextPageQuery = _.merge({}, query);
        nextPageQuery.next = results.lastSort;

        //Generate hypermedia
        results.nextPage = {
            '@type': 'sc:SearchAction',
            'sc:object': `sp:${env.uriPrefix}/${mapping[code]}`,
            'sc:target': {
                '@type': 'sc:EntryPoint',
                'sc:contentType': 'application/json',
                'sc:httpMethod': 'POST',
                'sc:urlTemplate': `sp:${env.uriPrefix}/v1/${mapping[code]}/_search`
            },
            'sc:query': nextPageQuery
        };

        delete results.lastSort;
    }

    res.body = results;

    res.set('Content-Type', 'application/json');
    res.status = 200;

    yield next;
};

const mapping = {
    'ITEMS': 'items',
};