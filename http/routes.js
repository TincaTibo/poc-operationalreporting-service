"use strict";

const koa = require('koa');
const cors = require('koa-cors');
const jwt = require('koa-jwt');
const compress = require('koa-compress');
const responseTime = require('koa-response-time');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const jwtDecode = require('jwt-decode');
const Q = require ('q');

const errorTitle = require('../utils/errorWithStatus').errorTitle;
const LD_Error = require('../models/ld_error');

const search = require('./search-service');

exports.initRoutes = function(config) {
    const app = koa();

    app.name = config.applicationName;
    app.context.config = config;
    app.context.log = config.log;

    app.use(logRequest());
    app.use(responseTime());
    app.use(errorHandler);
    app.use(compress());
    app.use(cors({
        origin: true,
        methods: ['GET','PUT','POST','PATCH','DELETE']
    }));

    initRouter(router, config);
    app.use(router.routes());

    app.use(jwt({
        secret: config.jwt.pubKey,
        algorithm: 'RS256',
        key: 'jwt',
    }).unless({
        path: '/'
    }));

    return Q.Promise(function (resolve, reject) {
        const server = app.listen(config.serverPort, (err) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(server); //returns the server
            }
        });
    });

    function *errorHandler(next) {
        try {
            yield next;
        }
        catch (err) {
            const res = this.response;
            //no specific error was already set
            //If headers were sent, default to default handler.
            if (res.headerSent) {
                this.app.context.log.error(
                    {err, req: this.req, res: this.res},
                    'Error happened while headers were already sent'
                );
                throw err;
            }
            else if (err.name === 'UnauthorizedError') { //Specific for JWT error
                const req = this.request;
                let invalidToken = false;
                if (req.headers['authorization']) {
                    const token = req.headers['authorization'].split(' ')[1];
                    try {
                        this.state.jwt = jwtDecode(token);
                    }
                    catch (e){
                        invalidToken = true;
                    }
                }
                this.app.context.log.error(
                    {
                        err,
                        req: this.req,
                        res: this.res,
                    },
                    `Error - Client does not have rights to access the service ${this.req && this.req.method} ${this.req && this.req.url}${invalidToken ? ' - Invalid token' : ''}`
                );
                res.status = 401;
                res.set('Content-Type', 'application/ld+json');
                res.body = new LD_Error(`Invalid security token`, `You do not have rights to access this service.`).stringify();
            }
            else if (err.status) {
                this.app.context.log.error(
                    {
                        err,
                        req: this.req,
                        res: this.res,
                    }
                );
                res.status = err.status;
                res.set('Content-Type', 'application/ld+json');
                res.body = new LD_Error(errorTitle(err.status), err.message).stringify();
            }
            else if (!err.status) {
                this.app.context.log.error(
                    {
                        err,
                        req: this.req,
                        res: this.res,
                    },
                    'Internal error 500'
                );
                res.status = 500;
                res.body = new LD_Error(res.status, err.message);
            }
        }
    }

    function logRequest() {
        let requestId = 0;
        return function*(next){
            this.state.requestId = ++requestId;
            this.app.context.log.debug({req: this.req}, `Received request: ${this.state.requestId}`);
            yield next;
            this.app.context.log.debug({req: this.req, res: this.res}, `Ended request: ${this.state.requestId} in ${this.response.get('X-Response-Time')}`);
        }
    }
};

function initRouter(router, config){

    router.get('/', function *(next){
        this.body = 'POC Read';
        yield next;
    });

    // LOGS

    router.post('/v1/items/_search/',
        bodyParser({
            jsonLimit: config.searchRequest.sizeLimitKB+'kb',
            extendTypes: { json: ['application/json'] }
        }),
        search.checkRequest,
        search.processRequest('ITEMS'));

    router.get('/v1/items/',
        bodyParser({
            jsonLimit: config.searchRequest.sizeLimitKB+'kb',
            extendTypes: { json: ['application/json'] }
        }),
        search.checkRequest,
        search.processRequest('ITEMS'));

    router.post('/items/_search/', function *(){
        this.redirect(`/v1/items/_search/`);
        this.status = 307;
    });

}